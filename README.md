# README #

This is a Debian package which installs an init script which generates a random hostname on boot.

## Installation ##

cd into the directory and run `debuild -uc -us -i`, then run `sudo dpkg -i` on the resulting `.deb` to install.

## Considerations ##

This package should never be used on machine that use networked fs as it is run before NFS mounts are available but requires executables in /usr/bin which would not be available at that time.